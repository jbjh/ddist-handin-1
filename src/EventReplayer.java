import javax.swing.JTextArea;
import java.awt.EventQueue;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;

/**
 * 
 * Takes the event recorded by the DocumentEventCapturer and replays
 * them in a JTextArea. The delay of 1 sec is only to make the individual
 * steps in the reply visible to humans.
 * 
 * @author Jesper Buus Nielsen
 *
 */
public class EventReplayer implements Runnable {

	private DocumentEventCapturer dec;
	private JTextArea area;
	
	private Socket socket;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	private boolean wasInterrupted;
	private boolean socketFailed = false;
	private Thread receiverThread;
	
	/**
	 * Constructor. Takes initial parameters, used by the class.
	 * @param dec - An instance of the DocumentEventCapturer class.
	 * @param area - Takes a JTextArea, for the text output.
	 * @param s - Takes a socket for the object streams.
	 */
	public EventReplayer(DocumentEventCapturer dec, JTextArea area, Socket s) {
		this.dec = dec;
		this.area = area;
		this.socket = s;
	}

	/**
	 * Called to interrupt the innerThread for recieving input.
	 * @throws IOException - Is propagated back to DistributedTextEditor class.
	 */
	protected void closeThread() throws IOException{
		wasInterrupted = true;
		receiverThread.interrupt();
		socket.close();
	}
	/**
	 * Check if socket failed
	 */
	public boolean checkSocket(){
		return socketFailed;
	}
	
	/**
	 * Runs the class thread. When called, it runs the receiver method and creates an outputstream for the socket.
	 * @exception ex
	 */
	public void run() {
		receiveInput();
		wasInterrupted = false; //Variable to check if interrupted.
		outputStream = null;

		try{
			outputStream = new ObjectOutputStream(socket.getOutputStream()); //Creates a new ObjectOutputStream for the socket.
		}catch(Exception ex){
//			ex.printStackTrace();
		}

		while (!wasInterrupted) { //Loops while not interrupted.
			try {
				MyTextEvent mte = dec.take(); //Get the head of the event queue.
				outputStream.writeObject(mte); //Write it to the ObjectOutputStream
				outputStream.flush(); //Flush the stream and loop again.

			}
			catch (SocketException sock){
				socketFailed = true;
				wasInterrupted = true;
//				sock.printStackTrace();
			}
			catch (Exception ex) {
//				ex.printStackTrace();
				wasInterrupted = true; //If an exception occurs, we interrupt the loop, and the thread dies.
				
			}
		}		
	}

	/**
	 * Receives input from the socket ObjectInputStream. 
	 * Takes each event in the queue and inserts it to the output JTextArea.
	 */
	public void receiveInput(){
		Runnable receiverRunnable = new Runnable(){
			public void run(){
				try {
					inputStream = new ObjectInputStream(socket.getInputStream()); //Creates a new ObjectInputStream for the socket.
					while (true){
						MyTextEvent mte = (MyTextEvent)inputStream.readObject(); //Reads the incoming objects.
						if (mte instanceof TextInsertEvent) { //Check for TextInsertEvent
							final TextInsertEvent tie = (TextInsertEvent) mte;
							EventQueue.invokeLater(new Runnable() {
								public void run() {
									try {
										area.insert(tie.getText(), tie.getOffset()); //Inserts to the output area, in the appropriate location.
									} catch (Exception ex) {
//										ex.printStackTrace();
										/* We catch all exceptions, as an uncaught exception would make the
										 * EDT unwind, which is not healthy.
										 */
									}
								}
							});
						} else if (mte instanceof TextRemoveEvent) { //Check for TextRemoveEvent
							final TextRemoveEvent tre = (TextRemoveEvent) mte;
							EventQueue.invokeLater(new Runnable() {
								public void run() {
									try {
										area.replaceRange(null, tre.getOffset(), tre.getOffset()+tre.getLength());
									} catch (Exception ex) {
//										ex.printStackTrace();
										/* We catch all exceptions, as an uncaught exception would make the
										 * EDT unwind, which is not healthy.
										 */
									}
								}
							});
						}
					}
				} catch (SocketException sock){
					socketFailed = true;
//					sock.printStackTrace();
				} catch (Exception ex) {
//					ex.printStackTrace();
				}
			}
		};
		receiverThread = new Thread(receiverRunnable); //Creates a new thread.
		receiverThread.start(); //Starts the new thread.
	}
}
