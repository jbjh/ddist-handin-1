import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.*;
import javax.swing.text.*;

public class DistributedTextEditor extends JFrame {

	private JTextArea textAreaInput = new JTextArea(20,120);
	private JTextArea textAreaOutput = new JTextArea(20,120);   
	private JTextField ipaddressfield = new JTextField("IP address here");     
	private JTextField portNumberfield = new JTextField("portnumber here");  
	private JFileChooser dialog = new JFileChooser(System.getProperty("user.dir"));
	
	private String currentFile = "Untitled";
	private boolean changed = false;
	
	private EventReplayer er;
	private Thread ert;   
	private boolean connected;
	private DocumentEventCapturer dec = new DocumentEventCapturer();

	protected boolean listen;
	
	InetAddress localhost;
	protected ServerSocket serverSocket;
	protected Socket clientSocket;
	protected int defaultPortNumber = 40304;
	protected int portNumber;

	/**
	 * Main constructor
	 * Creates the main GUI
	 */
	public DistributedTextEditor() {
		textAreaInput.setFont(new Font("Monospaced",Font.PLAIN,12));

		textAreaOutput.setFont(new Font("Monospaced",Font.PLAIN,12));
		((AbstractDocument)textAreaInput.getDocument()).setDocumentFilter(dec);
		textAreaOutput.setEditable(false);

		Container content = getContentPane();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

		JScrollPane scroll1 = 
				new JScrollPane(textAreaInput, 
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		content.add(scroll1,BorderLayout.CENTER);

		JScrollPane scroll2 = 
				new JScrollPane(textAreaOutput, 
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		content.add(scroll2,BorderLayout.CENTER);	

		content.add(ipaddressfield,BorderLayout.CENTER);	
		content.add(portNumberfield,BorderLayout.CENTER);	

		JMenuBar JMB = new JMenuBar();
		setJMenuBar(JMB);
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		JMB.add(file); 
		JMB.add(edit);

		file.add(Listen);
		file.add(Connect);
		file.add(Disconnect);
		file.addSeparator();
		file.add(Save);
		file.add(SaveAs);
		file.add(Quit);

		edit.add(Copy);
		edit.add(Paste);
		edit.getItem(0).setText("Copy");
		edit.getItem(1).setText("Paste");

		Save.setEnabled(false);
		SaveAs.setEnabled(false);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		textAreaInput.addKeyListener(k1);
		setTitle("Disconnected");
		setVisible(true);
		textAreaInput.insert("Text input area.", 0);
		textAreaOutput.insert("Text output area.", 0);
	}
	
	/**
	 * KeyListener attached to the input JTextArea.
	 */
	private KeyListener k1 = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			changed = true;
			Save.setEnabled(true);
			SaveAs.setEnabled(true);
		}
	};
	
	/**
	 * Helper method 
	 * Sets port number, to specified or reverts to default.
	 * @NumberFormatException - Thrown if no Integer can be parsed in the portNumberField.
	 */ 
	private void setPortNumber (){
		try{
			portNumber = Integer.parseInt(portNumberfield.getText()); //Throws NumberFormatException.
			if(portNumber > 65535 || portNumber < 1){ //Checks if portNumber is in range of the value limits.
				portNumber = defaultPortNumber; //Reverts to default port.
			}
		} catch (NumberFormatException nfe){
//			nfe.printStackTrace();
			portNumber = defaultPortNumber; //Reverts to default port.
		}
	}
	
	/**
	 * Helper method
	 * Clears input and output text areas.
	 */
	private void clearAreaFields(){
		textAreaInput.setText("");
		textAreaOutput.setText("");
	}
	
	/**
	 * Thread starter method, starts a new thread of the EventReplayer class.
	 * @param socket, containing the socket from connecting or listening.
	 */
	protected void startEventReplayer(Socket socket){
		er = new EventReplayer(dec, textAreaOutput, socket);
		ert = new Thread(er);
		ert.start();
		checkForDisconnect();
		connected = true;
	}
	
	/**
	 * Method to check for disconnects, it starts a new Thread to run in the background 
	 * checking if the socket failed at some point. If it did it recovers.
	 */
	protected void checkForDisconnect(){
		Runnable disconnectsRunnable = new Runnable(){
			public void run() {
				while(connected){
					ert.isAlive();
					if(er.checkSocket()){ //Checks if the EventReplayer had a socket exception.
						clearAreaFields();
						if(serverSocket != null){
							if(serverSocket.isClosed()){
								Disconnect.actionPerformed(null); //Forces a disconnect call to clear all sockets and threads.
							}
							else if (!serverSocket.isClosed()){
								setTitle("I'm listening on "+localhost.getHostAddress()+":"+portNumber
								+" - Client disconnected");
							}
							connected = false;
						} else if (clientSocket != null){
							Disconnect.actionPerformed(null); //Forces a disconnect call to clear all sockets and threads.
							connected = false;
						}
					}
				}
			}
		};
		Thread disconnectsThread = new Thread(disconnectsRunnable); //Creates a new thread.
		disconnectsThread.start(); //Starts the new thread.
	}

	/**
	 * Menu item - Listen
	 */
	Action Listen = new AbstractAction("Listen") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			clearAreaFields();
			listen = true;
			try {
				localhost = InetAddress.getLocalHost(); //Retrieves the local machines IP address.
				setPortNumber();
				serverSocket = new ServerSocket(portNumber); //Creates a new socket for the server.
				setTitle("I'm listening on "+localhost.getHostAddress()+":"+portNumber);			
			} 
			catch(BindException bind){ 
				setTitle("Disconnected - Try another port");
//				bind.printStackTrace();
			} 
			catch (Exception ex) {
//				ex.printStackTrace();
			} 	
			changed = false;
			Save.setEnabled(false);
			SaveAs.setEnabled(false);

			new Thread(){ //New thread to listen for incoming connections.
				public void run(){
					while(listen){ //Loops back to listen again, even if a connection is made. Used to recover from failed connections.
						Socket socket;
						try{
							socket = serverSocket.accept(); //Waits for incoming connection. Blocks until a connection is made.
							clearAreaFields();
							setTitle("I'm listening on  "
                                    +localhost.getHostAddress()
                                    +":"+portNumber 
                                    +" Client connected from "
                                    +socket.getRemoteSocketAddress());
							startEventReplayer(socket);
						}
						catch(Exception ex){
//							ex.printStackTrace();
							listen = false; //Stops listening if any exception is thrown.
						}
					}
				}
			}.start();
		}
	};

	/**
	 * Menu item - Connect
	 */
	Action Connect = new AbstractAction("Connect") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			clearAreaFields();
			setPortNumber();
			setTitle("Connecting to " + ipaddressfield.getText() + ":" + portNumber + "...");
			changed = false;
			Save.setEnabled(false);
			SaveAs.setEnabled(false);
			
			try{		
				clientSocket = new Socket(ipaddressfield.getText(), portNumber); //Creates a new socket for the client connection.
				if(clientSocket.isConnected()){ //Checks if the socket is connected.
					setTitle("Connected to " + ipaddressfield.getText() + ":" + portNumber);
					startEventReplayer(clientSocket);
				}
			//Catching exceptions thrown by socket connection issues. Sets title accordingly.
			}catch(UnknownHostException uhe){
				setTitle("Disconnected - Connection failed, try another host");
				//uhe.printStackTrace();
			}catch(ConnectException ce){
				setTitle("Disconnected - Connection timed out, try another host");
				//ce.printStackTrace();
			}catch(Exception ex){
				setTitle("Disconnected");
				//ex.printStackTrace();
			}
		}
	};

	/**
	 * Menu item - Disconnect
	 */
	Action Disconnect = new AbstractAction("Disconnect") {
		public void actionPerformed(ActionEvent e) {	
			setTitle("Disconnected");
			try {
				listen = false;
				if(er != null){ //Checks if eventReplayer is started and closes it if true.
					er.closeThread();					
					ert.interrupt();
				}
				if(clientSocket != null){ //If client socket exists. Close it.
					clientSocket.close();
				}
				if(serverSocket != null){ //If server socket exists. Close it.
					serverSocket.close();
				}
			} catch (Exception ex) { //Catches all exception.
//				ex.printStackTrace();
			}
		}
	};

	/**
	 * Menu item - Save
	 */
	Action Save = new AbstractAction("Save") {
		public void actionPerformed(ActionEvent e) {
			if(!currentFile.equals("Untitled"))
				saveFile(currentFile);
			else
				saveFileAs();
		}
	};

	/**
	 * Menu item - SaveAs
	 */
	Action SaveAs = new AbstractAction("Save as...") {
		public void actionPerformed(ActionEvent e) {
			saveFileAs();
		}
	};

	/**
	 * Menu item - Quit
	 */
	Action Quit = new AbstractAction("Quit") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			System.exit(0);
		}
	};
	
	ActionMap m = textAreaInput.getActionMap();
	
	Action Copy = m.get(DefaultEditorKit.copyAction);
	Action Paste = m.get(DefaultEditorKit.pasteAction);

	private void saveFileAs() {
		if(dialog.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
			saveFile(dialog.getSelectedFile().getAbsolutePath());
	}

	private void saveOld() {
		if(changed) {
			if(JOptionPane.showConfirmDialog(this, "Would you like to save "+ currentFile +" ?","Save",JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION)
				saveFile(currentFile);
		}
	}

	private void saveFile(String fileName) {
		try {
			FileWriter w = new FileWriter(fileName);
			textAreaInput.write(w);
			w.close();
			currentFile = fileName;
			changed = false;
			Save.setEnabled(false);
		}
		catch(IOException e) {
		}
	}

	/**
	 * Main method. Initializes the GUI.
	 * @param arg
	 */
	public static void main(String[] arg) {
		new DistributedTextEditor();
	} 
}
